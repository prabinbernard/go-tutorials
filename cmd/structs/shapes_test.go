// find perimeter of a rectangle
// formula 2(l+w)
package structs

import (
	"math"
	"testing"
)

const delta = 0.01

func assertMaths(t *testing.T, got, want float64, testName string) {
	if math.Abs(got-want) > delta {
		t.Errorf("%s: got %.2f want %.2f", testName, got, want)

	}
}
func TestPerimeter(t *testing.T) {
	rectangle := Rectangle{110.34, 64.67}
	got := rectangle.Perimeter()
	want := 350.02
	assertMaths(t, got, want, "Perimeter")
}

// write an area function for rectangles and circles
func TestArea(t *testing.T) {
	assertMath := func(t *testing.T, got, want float64) {
		t.Helper()

		if math.Abs(got-want) > delta {
			t.Errorf("got %.2f want %.2f", got, want)
		}
	}
	t.Run("Rectangles", func(t *testing.T) {
		rectangle := Rectangle{110.34, 64.67}
		got := rectangle.Area()
		want := 7135.69
		assertMath(t, got, want)

	})

	t.Run("Circles", func(t *testing.T) {
		circle := Circle{32.23}
		got := circle.Area()
		want := 3263.40
		assertMath(t, got, want)
	})

}
