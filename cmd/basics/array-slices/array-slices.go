package main

import "fmt"

// use to store multiple values of same type in one variable
var a = [5]int{1, 2, 3, 4, 5} // array, have fixed length
var b = []int{1, 2, 3}        // slice, flexible can grown and shrink

func main() {
	c := [2]string{"paul", "gomas"} // array, declaring and intialising in one line available only inside function
	d := []string{"lily", "maria"}  // slice, declaring and intialising in one line available only inside function
	fmt.Println(a, b)
	fmt.Println(c, d)
	//adding element into slice, copies the original array adds item and return a new slice
	e := append(d, "liya")
	fmt.Println(e)
	fmt.Println(d)
}
