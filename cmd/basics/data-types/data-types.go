package main

import "fmt"

// data type specifies the size and type of variable values
// there are three basic data types in golang bool, numeric, string
// blool represents boolean value of either true/false
// numeric represents integer, floating point and complex numbers
// string represent a string value

var a bool = true
var b int = 2
var c float64 = 45.67
var d string = "prabin"

func main() {
	fmt.Printf("%v, %d, %.2f, %s\n", a, b, c, d)
}
