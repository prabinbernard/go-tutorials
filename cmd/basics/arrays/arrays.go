package main

import (
	"fmt"
	"slices"
)

func main() {
	// arrays
	// ******
	var a [4]int // by default an array is zero valued
	fmt.Println(a)
	// // set values to the array
	a[0] = 100
	a[2] = 200
	// // get values from array
	fmt.Println(a[0], a[2])
	//decalare and initialise an array in one line
	b := [3]string{"a", "b", "c"}
	fmt.Println(b)
	// slices
	// *******
	var c []int // an unintialised slice equals to nil and has zero length
	fmt.Println(len(c))
	var d = make([]int, 3) // use make keyword to create an empty slice of non zero length.
	fmt.Println(d)
	//set values to a slice just like an array
	d[1] = 20
	d[2] = 40
	//get values from an array
	fmt.Println(d[1], d[2])
	fmt.Println(d)
	//decalre an initalise a slice in one line.
	e := []string{"john", "kevin"}
	fmt.Println(e)
	// slice can be appended to
	f := append(e, "mary")
	fmt.Println(f)
	g := append(d, 45)
	fmt.Println(g)
	// slice supports a slice operator [low:high]
	fmt.Println(g[1:3]) // slice up s[1] and s[2]
	fmt.Println(g[:3])  // slice up to but excluding s[3]
	fmt.Println(g[1:])  // slice up from including s[1]
	// slice can be copied to another slice
	var h = make([]string, len(e))
	copy(h, e)
	fmt.Println(h)
	// slices has many essential utilities from slices packages
	if slices.Equal(h, e) {
		fmt.Println("the slices are equal")
	}

}
