package iterations

import (
	"fmt"
	"reflect"
	"testing"
)

func TestAddArr(t *testing.T) {
	arr := []int{1, 2, 3, 4, 5}
	got := AddArr(arr)
	want := 15

	if got != want {
		t.Errorf("got %d want %d given %v", got, want, arr)
	}

}

// take any number of slices and return a new slice containig the totals of the slices passed in
func TestAddArrAll(t *testing.T) {
	arr := []int{1, 3, 5}
	brr := []int{2, 4, 6}
	crr := []int{4, 5, 4}
	got := AddArrAll(arr, brr, crr)
	want := []int{9, 12, 13}
	if !reflect.DeepEqual(got, want) {
		t.Errorf("got %v want %v", got, want)
	}
}

func ExampleAddArr() {
	arr := []int{1, 2, 3, 4}
	fmt.Println(AddArr(arr))
	// Output: 10
}

func ExampleAddArrAll() {
	arr := []int{2, 2}
	brr := []int{2, 6}
	fmt.Println(AddArrAll(arr, brr))
	// Output: [4 8]
}
