package iterations

import (
	"fmt"
	"testing"
)

func TestAddStr(t *testing.T) {
	got := AddStr("a", 3)
	expected := "aaa"

	if got != expected {
		t.Errorf("got %q expected %q", got, expected)
	}
}

func ExampleAddStr() {
	fmt.Println(AddStr("k", 5))
	// Output: kkkkk
}
