package iterations

func AddStr(str string, count int) string {
	var char string
	for i := 0; i < count; i++ {
		char += str
	}
	return char
}
