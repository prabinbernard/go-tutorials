package iterations

func AddArr(arr []int) int {
	var sum int = 0

	for _, num := range arr {
		sum += num
	}
	return sum
}

func AddArrAll(slices ...[]int) []int {
	var total []int
	for _, slice := range slices {
		total = append(total, AddArr(slice))
	}
	return total
}
