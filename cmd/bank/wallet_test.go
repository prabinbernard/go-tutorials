package bank

import (
	"testing"
)

func TestWallet(t *testing.T) {

	assertBalance := func(t *testing.T, got, want Bitcoin) {
		t.Helper()
		if got != want {
			t.Errorf("got %s want %s", got, want)
		}

	}

	assertError := func(t *testing.T, got error, want error) {
		t.Helper()
		if got == nil {
			t.Fatalf("wanted an error didnt get one")
		}

		if got != want {
			t.Errorf("got %q want %q", got, want)
		}

	}

	assertNoError := func(t *testing.T, got error) {
		t.Helper()
		if got != nil {
			t.Fatalf("wanted an error didnt get one")
		}

	}

	t.Run("Depost", func(t *testing.T) {
		wallet := Wallet{}
		wallet.Deposit(Bitcoin(10))

		got := wallet.Balance()
		want := Bitcoin(10)
		assertBalance(t, got, want)

	})

	t.Run("Withdraw", func(t *testing.T) {
		wallet := Wallet{}
		wallet.Deposit(Bitcoin(10))
		err := wallet.Withdraw(Bitcoin(10))
		got := wallet.Balance()
		want := Bitcoin(0)
		assertBalance(t, got, want)
		assertNoError(t, err)

	})

	t.Run("Withdraw insufficient funds", func(t *testing.T) {
		wallet := Wallet{balance: Bitcoin(20)}
		err := wallet.Withdraw(Bitcoin(100))
		got := wallet.Balance()
		want := Bitcoin(20)
		assertBalance(t, got, want)
		assertError(t, err, errInsufficientFunds)

	})

}
