package speaker

import "fmt"

type Speaker interface {
	Speak() string
}

type Person struct {
	Name string
}

type Student struct {
	Person
	School string
}

type Employee struct {
	Person
	Work   string
	Salary int
}

func (p Person) Speak() string {
	return fmt.Sprintf("Hello, I am %s", p.Name)
}

func (s Student) Speak() string {
	return fmt.Sprintf("Hello, I am %s, and I study in %s", s.Name, s.School)
}

func (e Employee) Speak() string {
	return fmt.Sprintf("Hello, I am %s, and I work at %s and receive a salary of %d$", e.Name, e.Work, e.Salary)
}
