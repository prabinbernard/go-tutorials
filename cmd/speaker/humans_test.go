package speaker

import "testing"

func TestSpeak(t *testing.T) {

	checkSpeak := func(t *testing.T, speaker Speaker, want string) {
		t.Helper()
		got := speaker.Speak()
		if got != want {
			t.Errorf("got %v want %v", got, want)
		}
	}

	t.Run("Person", func(t *testing.T) {
		speaker := Person{Name: "Paul"}
		want := "Hello, I am Paul"
		checkSpeak(t, speaker, want)

	})

	t.Run("Student", func(t *testing.T) {
		person := Person{Name: "Mary"}
		speaker := Student{Person: person, School: "MountCarmel"}
		want := "Hello, I am Mary, and I study in MountCarmel"
		checkSpeak(t, speaker, want)

	})

	t.Run("Employee", func(t *testing.T) {
		person := Person{Name: "Philip"}
		speaker := Employee{Person: person, Work: "Amazon", Salary: 100000}
		want := "Hello, I am Philip, and I work at Amazon and receive a salary of 100000$"
		checkSpeak(t, speaker, want)

	})
}
