package greetings

import "fmt"

func Greetings(name string) string {
	message := fmt.Sprintf("Hello %v", name)
	return message

}
