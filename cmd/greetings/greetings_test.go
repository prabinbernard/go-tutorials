package greetings

import "testing"

func TestGreetings(t *testing.T) {
	message := Greetings("Mathew")
	expected := "Hello Mathew"

	if message != expected {
		t.Errorf("got %q expected %q", message, expected)
	}
}
