package add

import (
	"fmt"
	"testing"
)

func TestAdd(t *testing.T) {
	sum := Add(1, 2)
	expected := 3

	if sum != expected {
		t.Errorf("got %d expected %d", sum, expected)
	}

}

func ExampleAdd() {
	sum := Add(3, 6)
	fmt.Println(sum)
	// Output: 9
}
