package main

import (
	"fmt"

	"gitlab.com/prabinbernard/go-tutorials/cmd/greetings"
	"gitlab.com/prabinbernard/go-tutorials/cmd/iterations"
	"gitlab.com/prabinbernard/go-tutorials/cmd/speaker"
	"gitlab.com/prabinbernard/go-tutorials/cmd/structs"
)

func main() {
	fmt.Println(greetings.Greetings("World!!!"))
	fmt.Println(iterations.AddArrAll([]int{1, 2}, []int{4, 5}))
	rectangle := structs.Rectangle{Length: 23.3, Width: 45.6}
	fmt.Println(rectangle.Area())
	p := speaker.Person{Name: "John"}
	s := speaker.Student{Person: speaker.Person{Name: "Maria"}, School: "Xaviers"}
	e := speaker.Employee{Person: speaker.Person{Name: "William"}, Work: "Google", Salary: 45000}
	fmt.Println(p.Speak())
	fmt.Println(s.Speak())
	fmt.Println(e.Speak())
	var i speaker.Speaker = s
	fmt.Println(i.Speak())

}
